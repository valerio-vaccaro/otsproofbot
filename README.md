# otsproofbot
The OtsProofBot is twitter bot useful for save and timestamp tweets using the OpenTimestamps protocol (https://opentimestamps.org/).

## Install

-Install MySQL/MariaDB than create the DB and table
-Install Node-RED
-Copy the flow and configure it
-Install and configure the node script (if you don't want use Twitter API)
