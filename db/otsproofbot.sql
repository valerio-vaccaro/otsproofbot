-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 12, 2019 at 02:17 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `otsproofbot`
--

-- --------------------------------------------------------

--
-- Table structure for table `TWEETS`
--

CREATE TABLE `TWEETS` (
  `ID` char(50) NOT NULL,
  `USER` tinytext NOT NULL,
  `TEXT` text NOT NULL,
  `TIME` tinytext NOT NULL,
  `OTS` text NOT NULL,
  `OTS_URL` tinytext NOT NULL,
  `STATUS` int(11) NOT NULL,
  `TWEET` longtext NOT NULL,
  `VIEWS` int(11) DEFAULT NULL,
  `JSON` text,
  `VERSION` int(11) DEFAULT NULL,
  `SOURCE` text,
  `test` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `TWEETS`
--
ALTER TABLE `TWEETS`
  ADD PRIMARY KEY (`ID`);
