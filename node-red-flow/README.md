# Node-red flow
[Node-RED](https://nodered.org/) is a programming tool for wiring together hardware devices, APIs and online services in new and interesting ways.

It provides a browser-based editor that makes it easy to wire together flows using the wide range of nodes in the palette that can be deployed to its runtime in a single-click.

## Install and run
In order to install Node-RED you can use the following command (based on NPM).

`npm i -g --unsafe-perm node-red`

## Import and configure the flow
Copy the code in the file flow.json in your Node-RED istance.
