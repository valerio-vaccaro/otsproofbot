# Reply to a tweet without using Twitter API
optsproofbot.js is a puppeteer script able to tweet without using API.

## Install
The script is based on node and puppeteer so in order to install use the following command.

`//npm install minimist puppeteer await-sleep`

## Configure
The script need configurations and request your tweeter username (without @), password and the phone number associated with the profile.

```
const USERNAME = ''
const PASSWORD = ''
...
const PHONE = '';
```

## Usage
In order to use the script you have to add the following arguments on command line:
-t: the text to tweet,
-u: the url of the tweet you want to reply to

`node otsproofbot.js -t="text" -u="url"`
