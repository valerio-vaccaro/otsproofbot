//npm install minimist puppeteer await-sleep

var argv = require('minimist')(process.argv.slice(2), {
  string: ['t']
});

const puppeteer = require('puppeteer')
const sleep = require('await-sleep')

const screenshot = new Date().toISOString() + ".png"
const USERNAME = ''
const PASSWORD = ''
const HTMLMESSAGE = argv.t
const URL = argv.u
const ID = URL.split('/')[5]
const PHONE = '';

(async() => {
  console.log(HTMLMESSAGE)
  console.log(URL)
  console.log(ID)

  const browser = await puppeteer.launch({
    headless: true,
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  })

  var page = await browser.newPage()
  await page.goto('https://twitter.com/login')
  await sleep(1000)
  await page.type('input[name="session[username_or_email]"]', USERNAME)
  await page.type('.js-password-field', PASSWORD)
  await page.click('button[type="submit"]')
  await page.waitForNavigation()
  await sleep(1000)

  phone = true;
  try {
    await page.waitForSelector('input#challenge_response.Form-textbox', {
      timeout: 5000
    })
  } catch (error) {
    phone = false;
  }

  if (phone) {
    await page.type('input#challenge_response.Form-textbox', PHONE)
    await page.click('input#email_challenge_submit.Button')
    await page.waitForNavigation()
    await sleep(1000)
  }

  page = await browser.newPage();
  await page.goto(URL)
  await page.waitForNavigation()
  await sleep(1000)

  await page.click('div#tweet-box-reply-to-' + ID)
  await sleep(500)

  await page.type('#tweet-box-reply-to-' + ID + ' div', HTMLMESSAGE)
  await sleep(500)

  await page.screenshot({
    path: "/opt/twitter/" + USERNAME + "_" + screenshot
  })

  await page.click(
    'form.t1-form.tweet-form.is-reply .TweetBoxToolbar .tweet-button button.tweet-action'
  )
  browser.close()
})()
